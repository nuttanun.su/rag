import streamlit as st
from search_engine import QDrantSearchEngine
from PyPDF2 import PdfReader
from typing import List, Dict
from embedding.huggingface import HuggingfaceEmbedding
from search_engine.faiss import FAISSSearchEngine
from docx import Document
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.text_splitter import CharacterTextSplitter
from reader.openai import OpenAiReader
from fixthaipdf import clean
from utils import get_pdf_text, read_text_file, read_docx_files, create_directory

from sentence_transformers import SentenceTransformer


TOKENIZERS_PARALLELISM= "false"


# with faiss
def main():
    st.title(" Vector Search App")

    st.sidebar.header("Import Documents File")
    files = st.sidebar.file_uploader(
        "Upload your PDFs here and click on Process", accept_multiple_files=True, type=["pdf", "txt", "docx"],
    )

    process_button = st.sidebar.button("Process File")

    if files is not None and process_button:
        with st.spinner('Processing Files'):
            for file in files:
                file_extension = file.name.split('.')[-1].lower()

                if file_extension == 'pdf':
                    raw_texts = get_pdf_text([file])
                elif file_extension == 'txt':
                    raw_texts = read_text_file([file])
                elif file_extension == 'docx':
                    raw_texts = read_docx_files([file])
                else:
                    st.error(f"Unsupported file format for {file.name}. Please upload a PDF, TXT, or DOCX file.")
            
            text_splitter = CharacterTextSplitter(
                separator = "\n",
                chunk_size = 1000,
                chunk_overlap  = 200,
                length_function = len,
                is_separator_regex = False,
            )
            raw_texts = clean(raw_texts)
            chunks = text_splitter.split_text(raw_texts)
            print("[texts] split chunks")
            # st.sidebar.write(chunks)

            search_engine = FAISSSearchEngine(
                embedding_function=HuggingfaceEmbedding(
                    model_name="intfloat/multilingual-e5-base"
                )
            )
            search_engine.index(chunks)

            create_directory("temp")
            search_engine.save_faiss(path="temp")
            print("[db] Indexed complete!")
            st.success('Indexed complete!')

    query_text = st.text_input("Enter your question ... ")
    option_km = st.sidebar.selectbox("knowledge-based from",
                        ("Documents", "Documents + External Sources"),
                        index=0,
        )
    
    model_name = st.sidebar.selectbox("Model",
                        ("gpt-3.5-turbo-1106", "gpt-4-1106-preview"),
                        index=0,
        )
    
    temperature = st.sidebar.slider("Temperature", 0.0, 2.0, 0.01)
    print(f"[system] mode:{option_km}, model :{model_name} temp:{temperature}")
    
    search_button = st.button("search")
    
    search_engine = FAISSSearchEngine(
        embedding_function=HuggingfaceEmbedding(
            model_name="intfloat/multilingual-e5-base"
        )
    )

    if query_text and search_button:
        with st.spinner('Processing '):
            search_engine.load_faiss()
            relavent_docs = search_engine.similarity_search(query_text)
            openai_reader = OpenAiReader(mode=option_km, model_name=model_name, temperature=temperature)
            result = openai_reader.generate_answer(query= query_text, chunks= relavent_docs)
            print(f"[reader] {result}")
            st.write(result)
            st.write(relavent_docs)


# with qdrant
# def main():
#     st.title("QDrant Vector Search App")

#     st.sidebar.header("Import Documents File")

#     files = st.sidebar.file_uploader(
#         "Upload your PDFs here and click on Process",
#         accept_multiple_files=True
#     )
#     process_button = st.sidebar.button("Process File")
#     search_engine = QDrantSearchEngine(collection_name= "demo-1",server_url= "http://localhost:6333",embedding_function=HuggingfaceEmbedding(model_name="intfloat/multilingual-e5-base"))

#     if files is not None and process_button:
#         for file in files:
#             file_extension = file.name.split('.')[-1].lower()

#             if file_extension == 'pdf':
#                 raw_texts = get_pdf_text([file])
#             elif file_extension == 'txt':
#                 raw_texts = read_text_file([file])
#             elif file_extension == 'docx':
#                 raw_texts = read_docx_files([file])
#             else:
#                 st.error(f"Unsupported file format for {file.name}. Please upload a PDF, TXT, or DOCX file.")

#         # text_splitter = CharacterTextSplitter(
#         #     separator = "|",
#         #     chunk_size = 1000,
#         #     chunk_overlap  = 200,
#         #     length_function = len,
#         #     is_separator_regex = False,
#         # )
#         # text_splitter = RecursiveCharacterTextSplitter(chunk_size = 500, chunk_overlap= 50)
#         clean_texts = clean(raw_texts)
#         chunks = clean_texts.split("|")
#         # print(chunks)
#         search_engine.index(chunks=chunks)
#         st.sidebar.write(chunks)
#         st.success("Indexing complete!")

#     query_text = st.text_input("Enter your query ... ")
#     search_button = st.button("Search")


#     if search_button:
#         relevant_docs = search_engine.similarity_search(query_text)
#         # print(relevant_docs[0].payload['text'])
#         openai_reader = OpenAiReader()
#         result = openai_reader.generate_answer(query= query_text, chunks= relevant_docs[0].payload['text'])
#         st.write(result)
#         for rel_doc in relevant_docs:
#             st.json({
#                     'text': rel_doc.payload['text'],
#                     'score': rel_doc.score,
#                     'filename': rel_doc.payload['chunk']
#             })

if __name__ == "__main__":
        main()
    # search_engine = QdrantSearchEngine()
    # reder
    # function 
    # run()