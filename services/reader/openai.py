from typing import List
from .base import Reader

import openai
from .config import OPENAI_API_KEY

openai.api_key = OPENAI_API_KEY
class OpenAiReader(Reader):
    def __init__(self, mode: str, model_name: str, temperature: float) -> None:
        self.mode = mode
        self.model_name =  model_name
        self.temperature = temperature

    def generate_answer(self, query: str, chunks: List[str]) -> str:
        if self.mode == "Documents":
            system_message_content = "Use the provided articles delimited by triple quotes to answer questions in the same language as the given input. If the answer cannot be found in the articles, write 'I could not find an answer.'"
        else:
            system_message_content = "Use the provided articles delimited by triple quotes to answer questions in the same language as the given input.If the answer is not present in the provided articles, warn me that the response is not sourced from the documents and provide the information obtained from the internet."
        response = openai.ChatCompletion.create(
            model = self.model_name,
            temperature= self.temperature,
            messages=[
            {
                "role": "system",
                "content": system_message_content
            },
            {
                "role": "user",
                "content": f"""{query}

Question: {chunks}"""
            }
        ]
        )
        # Extract and return the generated answer
        return response["choices"][0]["message"]["content"]
