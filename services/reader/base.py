from abc import ABC, abstractmethod
from typing import Any, List


class Reader(ABC):
    @abstractmethod
    def generate_answer(
        self,
        query: str,
        chunks: List[str],
        **kwargs
    ) -> str:
        ...

