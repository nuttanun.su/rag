from .base import Embedder
from typing import List
from sentence_transformers import SentenceTransformer

class HuggingfaceEmbedding(Embedder):
    def __init__(self, model_name: str) -> None:
        self.model_name = model_name
        self.model = SentenceTransformer(self.model_name)

    def embed(self, chunk: str) -> List[float]:
        vectors = self.model.encode(chunk).tolist()
        return vectors
    
    def get_sentence_embedding_dimension(self) -> int:
        dimension = self.model.get_sentence_embedding_dimension()
        return dimension