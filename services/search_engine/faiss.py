import os
import pickle
from typing import Any, List

import faiss
import numpy as np

from .base import SearchEngine
from embedding.huggingface import HuggingfaceEmbedding


class FAISSSearchEngine(SearchEngine):
    def __init__(self, embedding_function: Any, normalize_L2: bool =True) -> None:
        self.embedding_function = embedding_function
        self.vector_dimension = self.embedding_function.get_sentence_embedding_dimension()
        self.Index = faiss.IndexFlatL2(self.vector_dimension)
        self.texts = None
        self._normalize_L2 = normalize_L2
        print("INIT model")

    def index(self, chunks: List[str]) -> None:
        self.texts = chunks
        vectors = [self.embedding_function.embed(chunk) for chunk in chunks ] 
        vectors = np.array(vectors, dtype =np.float32)
        if self._normalize_L2:
            faiss.normalize_L2(vectors)
        self.Index.add(vectors)
        print("total index", self.Index.ntotal)

    def similarity_search_by_vector(self, vectors: List[float], k: int = 5) -> List[str]:
        vectors = np.array([vectors], dtype =np.float32)
        if self._normalize_L2:
            faiss.normalize_L2(vectors)
        distance, indices = self.Index.search(vectors, k)

        print(distance)
        docs = []
        for idx in indices[0]:
            docs.append(self.texts[idx])
        return docs

    def similarity_search(self, query: str, k: int = 5) -> List[str]:
        vectors = self.embedding_function.embed(query)
        relevant_docs = self.similarity_search_by_vector(
            vectors= vectors,
            k = k 
        )
        return relevant_docs

    def save_faiss(self, path: str = "temp", index_name: str = "index") -> None:
        if not os.path.exists(path):
            os.makedirs(path)

        faiss.write_index(
            self.Index, "%s/%s.faiss" % (path, index_name)
        )
        with open("%s/%s.pkl" % (path, index_name), "wb") as f:
            pickle.dump(self.texts, f)

    def load_faiss(self, path: str = "temp", index_name: str = "index") -> None:
        self.Index = faiss.read_index(
            "%s/%s.faiss" % (path, index_name)
        )
        with open("%s/%s.pkl" % (path, index_name), "rb") as f:
            self.texts = pickle.load(f)