from typing import Any, List
import os
import requests

from qdrant_client import QdrantClient
from qdrant_client.http.models import VectorParams, Distance, PointStruct

from .base import SearchEngine
from embedding.huggingface import HuggingfaceEmbedding


os.environ['QDRANT_HOST'] = ''

class QDrantSearchEngine(SearchEngine):
    def __init__(self, collection_name: str, embedding_function: Any ,server_url: str = "http://localhost:6333") -> None:
        self.server_url = server_url
        self.qdrant_client = QdrantClient(self.server_url)
        self.embedding_function = embedding_function
        self.collection_name = collection_name
        self.filename = None
        self.texts = None

    def index(self, chunks: List[str],filename: str = None ,metadata: List[Any] = None) -> None:
        self.texts = chunks
        self.filename = filename
        vectors = [self.embedding_function.embed(chunk) for chunk in chunks]
        self.create_collection()
        self.upload_vectors(vectors=vectors)

    # def create_collection(self, collection_name: str, dim_size: int, ) -> None:
    def create_collection(self,) -> None:
        self.qdrant_client.recreate_collection(
            collection_name= self.collection_name,
            vectors_config= VectorParams(
                size = 768, # need to change
                distance = Distance.COSINE
            )
        )
    
    def upload_vectors(self, vectors: List[List[float]]) -> None:
        for idx , vector in enumerate(vectors):
            self.qdrant_client.upsert(
                collection_name=self.collection_name,
                wait=True,
                points=[
                    PointStruct(
                        id = idx,
                        vector = vector,
                        payload = {
                            "text": self.texts[idx],
                            "chunk": f"{self.filename} +[{idx}]"
                        }
                    )
                ]
            )

    def similarity_search_by_vector(self, vectors:List[float], k: int = 5) -> List[str]:
        results = self.qdrant_client.search(
            collection_name=self.collection_name, 
            query_vector=vectors,
            limit = k 
        )
        return results

    def similarity_search(self, query: str, k: int = 10) -> List[str]:
        vectors = self.embedding_function.embed(query)
        relevant_docs = self.similarity_search_by_vector(
            vectors= vectors,
            k = k 
        )
        return relevant_docs