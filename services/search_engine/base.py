from abc import ABC, abstractmethod
from typing import List


class SearchEngine(ABC):
    @abstractmethod
    def index(self, chunk: str) -> None:
        ...

    @abstractmethod
    def similarity_search(self, query: str) -> List[str]:
        ...