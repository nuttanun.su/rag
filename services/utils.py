import os
import re

from PyPDF2 import PdfReader
from typing import List, Dict
from docx import Document

def get_pdf_text(pdf_docs):
    texts = ""
    for pdf in pdf_docs:
         pdf_reader = PdfReader(pdf)
         for page in pdf_reader.pages:
              texts += page.extract_text() +"\n"
    return texts

def read_text_file(txt_docs):
    print(type(txt_docs[0]))
    texts = ""
    for doc in txt_docs:
        content = doc.getvalue().decode("utf-8")
        texts += content + ("\n")
    return texts

def read_docx_files(docs_files):
    texts = ""
    
    for doc_file in docs_files:
        # Assuming doc_file is a file-like object, adjust this if needed
        doc = Document(doc_file)

        # Access paragraphs
        for paragraph in doc.paragraphs:
            texts += paragraph.text + "\n"
        print(texts)
    return texts

def create_directory(directory_path):
    if not os.path.exists(directory_path):
        os.mkdir(directory_path)
        print(f"Directory '{directory_path}' created successfully.")
    else:
        print(f"Directory '{directory_path}' already exists.")
